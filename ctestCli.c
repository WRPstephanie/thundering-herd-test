//
// Created by wrp on 2023/6/8.
//
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>

int main(){

    int mySocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    printf("Socket: %d\n",mySocket);

    //连接服务器
    struct sockaddr_in serverAddr;
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(8398);
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

    connect(mySocket, (struct sockaddr*)&serverAddr, sizeof(struct sockaddr));
    printf("connected server");

    //关闭连接
    close(mySocket);

    return 0;
}
