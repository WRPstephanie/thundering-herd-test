//
// Created by 廖肇燕 on 2023/2/14.
//

#ifndef UNITY_LOCAL_BEAVER_H
#define UNITY_LOCAL_BEAVER_H

#define NATIVE_EVENT_MAX 64

typedef struct native_event {
    int fd;
    short int ev_in;
    short int ev_out;
    short int ev_close;
}native_event_t;

typedef struct native_events {
    int num;
    native_event_t evs[NATIVE_EVENT_MAX];
}native_events_t;

int init(int listen_fd);
void deinit(int efd);
int add_fd(int efd, int fd);
int mod_fd(int efd, int fd, int wr);
int del_fd(int efd, int fd);
int poll_fds(int efd, int tmo, native_events_t* nes);

#endif //UNITY_LOCAL_BEAVER_H
